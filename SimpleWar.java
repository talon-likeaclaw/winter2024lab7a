public class SimpleWar {
	
	public static void main(String[] args) {
		// Create and shuffle deck
		Deck newDeck = new Deck();
		newDeck.shuffle();
		
		// Variables
		int cardsRemaining = newDeck.getNumOfCards();
		int playerOnePoints = 0;
		int playerTwoPoints = 0;
		double firstCardScore = 0.0;
		double secondCardScore = 0.0;
		
		
		// MAIN GAME LOOP
		while (cardsRemaining > 0) {
			// Print points before round.
			System.out.println("Player One's Points: " + playerOnePoints);
			System.out.println("Player Two's Points: " + playerTwoPoints);
			
			
			// Draw two cards and calculate their score.
			Card firstCard = newDeck.drawTopCard(1);
			Card secondCard = newDeck.drawTopCard(1);
			firstCardScore = firstCard.calculateScore();
			secondCardScore = secondCard.calculateScore();
			// Print cards.
			System.out.println("Player One's Card: " + firstCard);
			System.out.println("Player Twos's Card: " + secondCard);
			// Compare scores and add points.
			if (firstCardScore > secondCardScore) {
				System.out.println("Player One Wins This Round!");
				playerOnePoints++;
			} else {
				System.out.println("Player Two Wins This Round!");
				playerTwoPoints++;
			}
			// Calculate number of cards left in deck and print points after round.
			cardsRemaining = newDeck.getNumOfCards();
			System.out.println("Player One's Points: " + playerOnePoints);
			System.out.println("Player Two's Points: " + playerTwoPoints + "\n");
		}
		// Compare player scores and indicate who won the game.
		if (playerOnePoints > playerTwoPoints) {
			System.out.println("Congrats Player One, You Win!!");
		} else if (playerTwoPoints > playerOnePoints) {
			System.out.println("Congrats Player Two, You Win!!");
		} else {
			System.out.println("It's a draw!! Good job everyone!! :)");
		}
		
	}
	
}