import java.util.Random;

public class Deck {
	
	// fields
	private Card[] cards;
	private int numberOfCards;
	private Random rng;
	
	// constructor
	public Deck() {
		this.numberOfCards = 52;
		this.rng = new Random();
		this.cards = new Card[52];
		// Initalize suits and values of cards with arrays.
		String[] suits = new String[] {"Spade", "Club", "Diamond", "Heart"};
		String[] values = new String[] {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13"};
		int index = 0;
		// Initilize each card with a unique suit and value.
		for (int s = 0; s < suits.length; s++) {
			for(int v = 0; v < values.length; v++) {
				this.cards[index] = new Card(suits[s], values[v]);
				index++;
			}
		}
	}
	
	// getters
	public int getNumOfCards() {
		return this.numberOfCards;
	}
	
	// toString()
	public String toString() {
		String deckString = "";
		for (int i = 0; i < numberOfCards; i++) {
			deckString += cards[i] + " \n";
		}
		return deckString;
	}
	
	// methods
	public int length() {
		return this.numberOfCards;
	}
	
	// Draw the top card and reduce the number of cards in deck.
	public Card drawTopCard(int numRemove) {
		this.numberOfCards = this.numberOfCards - numRemove;
		return this.cards[this.numberOfCards];
	}
	
	// Shuffle the deck of cards
	public void shuffle() {
		for (int i = 0; i < numberOfCards; i++) {
			int random = rng.nextInt(numberOfCards);
			Card storage = this.cards[random];
			this.cards[random] = this.cards[i];
			this.cards[i] = storage;
		}
	}
	
}