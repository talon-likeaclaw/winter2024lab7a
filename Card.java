public class Card {
	
	// fields
	private String suit;
	private String value;
	
	// constructor
	public Card(String suit, String value) {
		this.suit = suit;
		this.value = value;
	}
	
	// getters
	public String getSuit() {
		return this.suit;
	}
	
	public String getValue() {
		return this.value;
	}
	
	// toString()
	public String toString() {
		return this.value + " of " + this.suit + "s";
	}
	
	// Calculate Score of card.
	public double calculateScore() {
		double score = 0.0;
		int value = Integer.parseInt(this.value);
		double suit = 0.0;
		// Determine decimal value with suit value.
		if (this.suit == "Heart") {
			suit = 0.4;
		} else if (this.suit == "Diamond") {
			suit = 0.3;
		} else if (this.suit == "Club") {
			suit = 0.2;
		} else {
			suit = 0.1;
		}
		score = value + suit;
		return score;
	}
}